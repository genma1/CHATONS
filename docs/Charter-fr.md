
# Charte des CHATONS

## Le Collectif

CHATONS est un Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires. Chaque membre de ce collectif, ci-après dénommé « CHATON », s’engage à respecter la présente charte.

Ce qui n’est pas interdit explicitement par la présente charte ou par la loi, est autorisé.

>  **Critères techniques associés :**
>  - le CHATON peut être de n’importe quelle forme juridique : personne morale ou personne physique (un particulier, une entreprise, une association, une SCOP, etc.).
> 
> **Critères éthiques et sociaux associés :**
> - le CHATON s’engage à agir dans le respect et la bienveillance vis-à-vis des autres membres du collectif ;
> - le CHATON s’engage à agir dans le respect et la bienveillance vis-à-vis des utilisateurs de ses services.

## Hébergeurs

Un hébergeur est ici défini comme une entité hébergeant des données fournies par des tiers et proposant des services par lesquels transitent ou sont stockées ces données.
Les utilisateurs ci-après dénommés « hébergés » sont les personnes, physiques ou morales, ayant la possibilité de créer ou modifier des données sur l’infrastructure du CHATON.

>  **Critères techniques associés :**
>  - le CHATON doit avoir le contrôle sur les logiciels et les données associées (accès *root*) ;
> - le CHATON s’engage à afficher publiquement et sans ambiguïté son niveau de contrôle sur le matériel hébergeant les services et les données associées. Notamment, le nom du fournisseur ou prestataire d’hébergement physique des serveurs devra clairement être indiqué aux hébergés.
> 
> **Critères éthiques et sociaux associés :**
> - le CHATON s’engage, pour tous les services concernant directement ou indirectement les hébergés, à ne pas utiliser les services de prestataires tiers réputés comme ayant une utilisation dévoyée des données personnelles de leurs clients *[en gros, interdiction d’utiliser les services Amazon Web Services, Dropbox et autres Google Drive]*

## Alternatifs

En proposant des services libres en ligne, le CHATON se propose d’être une alternative au modèle des entreprises proposant des services fermés et centralisés à des fins de monopole et d’usage dévoyé des données personnelles.

Ces alternatives illustrent la diversité des solutions libres disponibles et pouvant être mises à disposition à des fins personnelles ou collectives. 

>  **Critères techniques associés :**
>  - le CHATON s’engage à produire de la documentation sur son infrastructure et ses services ;
> - le CHATON s’engage à ne pas mettre en place de dispositif de suivi des usages des services proposés autres qu’à des fins statistiques ou administratives.
> 
> **Critères éthiques et sociaux associés :**
> - le CHATON s’engage à prioriser les libertés fondamentales de ses utilisateurs (notamment le respect de leur vie privée), dans chacune de ses actions ;
> - le CHATON ne devra en aucun cas utiliser les services de régies publicitaires. Le sponsoring ou le mécénat, par le biais d’affichage de l’identité de structures partenaires (nom, logo, etc.) est autorisée, à condition qu’aucune information à caractère personnel ne soit communiquée aux partenaires ;
> - le CHATON ne devra faire aucune exploitation commerciale des données ou métadonnées des hébergés.


## Transparents

Le CHATON assure aux hébergés qu’aucun usage déloyal ne sera fait de leurs données, de leur identité ou de leurs droits, notamment par le biais de conditions générales d’utilisation claires, concises et facilement accessibles.

C’est en connaissance de cause que les utilisateurs pourront user des services disponibles et prendre connaissance de leurs fonctionnalités, de leur avantages, de leurs limites et de leurs emplois.

Aucune donnée produite à l’aide des services n’appartient au membre du CHATONS. Le membre n’y appose aucun droit et n’y pratique aucune censure tant que les contenus respectent ses Conditions Générales d’Utilisation.

>  **Critères techniques associés :**
> - le CHATON s’engage à être transparent sur son infrastructure technique (distribution et logiciels utilisés). Pour des raisons de sécurité, certaines informations (numéro de version de logiciel, par exemple) pourront ne pas communiquées publiquement ;
> - le CHATON s’engage à faire de son mieux (obligation de moyens) en terme de sécurisation de son infrastructure ;
> - le CHATON s’engage à mettre en place la politique de sauvegarde qui lui parait la plus adaptée ;
> - le CHATON s’engage à publier des statistiques d’usage anonymisées ;
> - le CHATON s’engage à publier un journal des incidents techniques des services.
> 
> **Critères éthiques et sociaux associés :**
> - le CHATON s’engage à ne s’arroger aucun droit de propriété sur les contenus, données et métadonnées produits par les hébergés ou les utilisateurs ;
> - le CHATON s’engage à indiquer publiquement quelle est son offre de service, ainsi que les tarifs associés ;
> - le CHATON s’engage à publier des Conditions Générales d’Utilisations (CGU) visibles, claires, non ambigues, et compréhensibles par le plus grand nombre ;
> - le CHATON s’engage à publier, dans ses CGU, une clause « Données personnelles et respect de la vie privée » indiquant clairement quelle est la politique du CHATON concernant les pratiques visées ;
> - le CHATON s’engage à rendre publics ses comptes et rapports d’activités (au moins pour la partie concernant son activité de CHATON) ;
> - le CHATON s’engage à porter à la connaissance des hébergés les principales informations concernant sa politique de sécurité et de sauvegarde (en veillant évidemment à ce que ses informations ne portent pas atteinte à ladite politique de sécurité) ;
> - le CHATON s’engage à communiquer avec les hébergés sur les difficultés qu’il rencontre dans l’exploitation de sa structure et des différents services qu’il met à disposition *[ex: envoyer un mail lorsque les pads sont en rade, répondre aux utilisateurs, etc.]*

## Ouverts 

L’utilisation des logiciels libres et des standards ouverts sur Internet est le moyen exclusif par lequel les membres proposent leurs services.

L’accès au code source est au fondement des principes du Libre. Pour chaque service qu’il propose, le membre s’engage à ce qu’il utilise des logiciels ou des éléments de code placés exclusivement sous licence libre.

En cas d’amélioration du code des logiciels utilisés, le membre s’engage à placer ses contributions sous licence libre (compatible avec la licence initiale) et encouragera toute contribution volontaire de la part des utilisateurs en les invitant à contacter les auteurs.

Le membre s’engage à rendre accessible le code source soit en publiant un lien vers le site officiel de l’application, soit, si ce dernier n’est plus disponible, en publiant le code utilisé.

>  **Critères techniques associés :**
> - le CHATON s’engage à utiliser exclusivement des distributions libres (GNU/Linux, FreeBSD, etc.) comme système d’exploitation pour l’infrastructure des hébergés
> - le CHATON s’engage à utiliser exclusivement des logiciels soumis à des licences libres, au sens de la Free Software Foundation, qu’elles soient compatibles ou non avec la licence GPL (liste disponible ici : http://www.gnu.org/licenses/license-list.html)
> - le CHATON s’engage à n’utiliser que des formats ouverts
> - le CHATON s’engage, en cas de modification de code source des logiciels utilisés, à rendre publique ses modifications (sauf si celles-ci peuvent porter atteinte à la sécurité ou à la vie privée des utilisateurs).
> **Critères éthiques et sociaux associés :**
> - le CHATON s’engage à contribuer, techniquement ou financièrement, au mouvement du logiciel libre. Ses contributions seront listées publiquement *[expliquer qu’il ne s’agit pas d’une compétition (peu importe l’importance de la contribution), mais que le libre dépend des contributions, et qu’il faut donc participer et le faire savoir ?]*
> - le CHATON s’engage à faciliter la possibilité pour les hébergés à quitter ses services avec les données associées dans des formats ouverts
> - le CHATON s’engage à supprimer définitivement toute information (compte et données personnelles) concernant l’hébergé à la demande de ce dernier.


## Neutres

L’éthique du logiciel libre est faite de partage et d’indépendance. Le membre du CHATONS s’engage à ne pratiquer aucune censure a priori des contenus, aucune surveillance des actions des utilisateurs, et à ne répondre à aucune demande administrative ou d’autorité sans une requête légale présentée en bonne et due forme. En retour, les utilisateurs s’engagent, dans la production ou l’hébergement de leurs contenus, à respecter les cadres prévus par la loi du pays concerné.

L’égalité de l’accès à ces applications est un engagement fort : qu’elles soient payantes, premium ou gratuites les offres des membres du CHATONS doivent se faire sans discrimination technique ou sociale, et offrir toutes les garanties de la neutralité concernant la circulation des données.

Le respect de la vie privée des utilisateurs est un besoin impérieux. Aucune donnée personnelle ne sera exploitée à des fins commerciales, transmise à des tiers ou utilisée à des fins non prévues par la présente charte, excepté pour des besoins statistiques et toujours dans le respect du cadre légal.

>  **Critères techniques associés :**
> - le CHATON s’engage à ne pratiquer aucune surveillance des actions des utilisateurs ;
> - le CHATON s’engage à ne pratiquer aucune censure *a priori* des contenus des hébergés ;
> - le CHATON s’engage à protéger au mieux les données de ses utilisateurs des atteintes extérieures, notamment en utilisant autant que possible un chiffrement fort des données, que cela soit sur le réseau, ou sur les supports de stockage. Le [chiffrement de bout-en-bout](https://en.wikipedia.org/wiki/End-to-end_encryption)  des données et métadonnées, lorsqu’il est possible, doit être privilégié. *[bon, clairement, ça ne va pas être simple, mais il faut l’afficher comme un objectif]*
> 
> **Critères éthiques et sociaux associés :**
> - le CHATON s’engage à ne pas répondre à une requête administrative ou d’autorité nécessitant la communication d’informations personnelles avant que ne lui soit présentée une requête légale en bonne et dûe forme ;
> - le CHATON s’engage à tenir une liste, à destination de ses hébergés, des requêtes administratives ou d’autorité qui lui auraient été présentées *[idée: tenir un journal des requêtes. Sinon, utiliser un [canari](https://en.wikipedia.org/wiki/Warrant_canary) ?]* ;
> - le CHATON s’engage, si une action de modération s’avère nécessaire, à appliquer ses CGU avec bienveillance.


## Solidaires

Dans une démarche de partage et d’entraide, chaque membre diffuse à destination des autres membres et du public un maximum de connaissances pour promouvoir les usages des logiciels libres et apprendre à installer des services libres en ligne. Ce partage des ressources, techniques et cognitives, fait d’Internet un bien commun, disponible pour tous et n’appartenant à personne.

>  **Critères techniques associés :**
> - le CHATON s’engage à prévoir structurellement la possibilité de rencontrer, échanger, et faire participer les hébergés *[agenda, apéro, forum, etc. Bref, ne pas rester dans un entre-soi.]* ;
> - le CHATON s’engage à définir un modèle économique basé sur la solidarité *[je sais, c’est vague... l’idée, c’est que les coûts de structure doivent reposer essentiellement sur les contributions des usagers]* ;
> - le CHATON s’engage à avoir une démarche active et volontaire en terme d’accès pour tous aux services proposés, notamment en respectant les normes [d’accessibilité web](https://fr.wikipedia.org/wiki/Accessibilit%C3%A9_du_web#WCAG_2.0).
> 
>  **Critères éthiques et sociaux associés :**
> - le CHATON s’engage à ne pas exclure *a priori* de potentiels souscripteurs aux services proposés. Le CHATON pourra définir des « publics cibles » auquel il souhaite s’adresser (par exemple sur des critères de proximité géographique, ou de centres d’intérêts), cependant il devra, autant que possible, répondre aux demandes non pertinentes, par exemple en les orientant vers un autre CHATON ou en facilitant l’émergence de structures qui répondraient aux besoins non-satisfaits.a
> - le CHATON s’engage à diffuser le plus largement possible ses connaissances. *[Au-delà de l’aspect « transparence », l’enjeu est ici d’éviter à d’autres de réinventer la roue.]*
> - le CHATON s’engage à faciliter la participation des hébergés au maintien des services. Une attention particulière sera apportée aux « non-techniciens » afin de leur permettre d’accroître leurs savoirs et leurs compétences  et de pouvoir jouer ainsi pleinement leur rôle de contributeurs.
> - le CHATON s’engage à faciliter l’émancipation des hébergés et des publics qu’il souhaite toucher, notamment au travers de démarches d’éducation populaire (rencontres, formations internes ou externes, chiffrofêtes, temps de rédaction collaborative de documentations, hackathon, etc.) afin de s’assurer qu’Internet demeure une technologie abordable par tous, pour tous.